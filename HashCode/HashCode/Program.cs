﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HashCode
{
    class Program
    {

        static List<Endpoint> Endpoints = new List<Endpoint>();
        public static List<Video> Database = new List<Video>();
        static List<Request> Requests = new List<Request>();
        public static Cache[] Cashes;
        static List<int> BadCaches;
        static int NumberOfVideos;
        static int NumberOfEndpoints;
        static int NumberOfRequests;
        static int NumberOfCaches;
        static int SizeOfCache;
        static int LineNr = 0;
        static void Main(string[] args)
        {
            ReadData("Kittens.in");
            SortRequests();
            PutToCaches();
            PrintToFile("kittensRez");
            LineNr = 0;
            Endpoints = new List<Endpoint>();
            Database = new List<Video>();
            Requests = new List<Request>();
            ReadData("me_at_the_zoo.in");
            SortRequests();
            PutToCaches();
            PrintToFile("ZooRez");
            LineNr = 0;
            Endpoints = new List<Endpoint>();
            Database = new List<Video>();
            Requests = new List<Request>();
            ReadData("trending_today.in");
            SortRequests();
            PutToCaches();
            PrintToFile("TrendingRez");
            LineNr = 0;
            Endpoints = new List<Endpoint>();
            Database = new List<Video>();
            Requests = new List<Request>();
            ReadData("videos_worth_spreading.in");
            SortRequests();
            PutToCaches();
            PrintToFile("SpreadingRez");

        }
        static void ReadData(string path)
        {
            string[] allData = File.ReadAllLines(path);
            string[] line = allData[0].Split(' ');
            NumberOfVideos = int.Parse(line[0]);
            NumberOfEndpoints = int.Parse(line[1]);
            NumberOfRequests = int.Parse(line[2]);
            NumberOfCaches = int.Parse(line[3]);
            Cashes = new Cache[NumberOfCaches];
            SizeOfCache = int.Parse(line[4]);
            line = allData[1].Split(' ');
            for (int i = 0; i < NumberOfVideos; i++)
            {
                Database.Add(new Video(int.Parse(line[i]), i));
            }
            for (int i = 0; i < NumberOfEndpoints; i++)
            {
                line = allData[2 + LineNr].Split(' ');
                Endpoints.Add(new Endpoint(i, int.Parse(line[0]), int.Parse(line[1])));
                LineNr++;
                int x = int.Parse(line[1]);
                for (int j = 0; j < x; j++)
                {
                    line = allData[2 + LineNr].Split(' ');

                    Endpoints[i].Caches.Add(new Cache(int.Parse(line[1]), SizeOfCache, int.Parse(line[0])));


                    Cashes[int.Parse(line[0])] = Endpoints[i].Caches[j];

                    LineNr++;
                }

            }
            for (int i = 0; i < NumberOfRequests; i++)
            {
                line = allData[2 + LineNr].Split(' ');
                LineNr++;
                Requests.Add(new Request(int.Parse(line[0]), int.Parse(line[2]), Endpoints[int.Parse(line[1])]));
                Requests[i].GetBestRoute();
                Requests[i].CalculateKof();
            }
        }
        static void SortRequests()
        {
            Requests = Requests.OrderByDescending(x => x.Kof).ToList();
        }
        static void PutToCaches()
        {
            foreach (Request request in Requests)
            {
                BadCaches = new List<int>();
                PutToCache(request);

            }
        }
        static void PutToCache(Request request)
        {

            if (request.Caches.Count > 0)
            {

                if (Cashes[request.BestCache].Size > Database[request.VideoId].Size && !Cashes[request.BestCache].Videos.Contains(Database[request.VideoId]))
                {
                    Cashes[request.BestCache].Videos.Add(Database[request.VideoId]);
                    Cashes[request.BestCache].Size -= Database[request.VideoId].Size;
                }
                else
                {

                    BadCaches.Add(request.BestCache);
                    if (request.Count < request._Endpoint.NumberOfCaches)
                    {
                        request.Count++;

                        request.GetAlternateCache(BadCaches);
                        PutToCache(request);
                    }
                    else
                        Console.WriteLine("Recursion end" + request.VideoId);
                }
            }
        }
        static int FindUsedCaches()
        {
            int x = 0;
            foreach (Cache cache in Cashes)
            {

                if (cache.Videos.Count > 0)
                {
                    x++;
                }

            }
            return x;
        }
        static void PrintToFile(string name)
        {
            using (StreamWriter writer = new StreamWriter(name + ".txt"))
            {
                writer.WriteLine(FindUsedCaches().ToString());
                foreach (Cache cache in Cashes)
                {
                    if (cache.Videos.Count > 0)
                    {
                        writer.Write(cache.Id);
                        foreach (Video video in cache.Videos)
                        {
                            writer.Write(" " + video.Id);
                        }
                        writer.WriteLine();
                    }
                }
            }
        }
    }
}
