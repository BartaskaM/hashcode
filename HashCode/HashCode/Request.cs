﻿using System.Collections.Generic;

namespace HashCode
{
    class Request
    {
        public double Kof;
        public Endpoint _Endpoint;
        public List<Cache> Caches = new List<Cache>();

        public int VideoId;
        public int NumberOfRequests;
        public int ShortestRoute;
        public int BestCache = 0;
        public int Count = 0;
        public List<int> BadCaches = new List<int>();
        public Request(int videoId, int numberOfRequests, Endpoint endpoint)
        {
            _Endpoint = endpoint;
            NumberOfRequests = numberOfRequests;
            VideoId = videoId;
            Caches = endpoint.Caches;
        }
        public void CalculateKof()
        {
            Kof = NumberOfRequests * (_Endpoint.LatencyToDatabase - GetBestRoute());
        }
        public int GetBestRoute()
        {

            ShortestRoute = _Endpoint.LatencyToDatabase;
            foreach (Cache cache in Caches)
            {

                if (cache.LatencyToEndpoint < ShortestRoute && cache.Size > Program.Database[VideoId].Size)
                {
                    ShortestRoute = cache.LatencyToEndpoint;
                    BestCache = cache.Id;
                }

            }
            return ShortestRoute;
        }
        public void GetAlternateCache(List<int> badCaches)
        {
            ShortestRoute = _Endpoint.LatencyToDatabase;
            for (int i = badCaches[badCaches.Count - 1]; i < Caches.Count; i++)
            {
                if (Caches[i].LatencyToEndpoint < ShortestRoute)
                {
                    ShortestRoute = Caches[i].LatencyToEndpoint;
                    BestCache = Caches[i].Id;
                }
            }
        }
    }
}
