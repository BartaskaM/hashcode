﻿using System.Collections.Generic;

namespace HashCode
{
    class Cache
    {
        public int LatencyToEndpoint;
        public int Size;
        public int Id;
        public List<Video> Videos = new List<Video>();
        public Cache(int latency, int size, int id)
        {
            LatencyToEndpoint = latency;
            Size = size;
            Id = id;
        }
    }
}
