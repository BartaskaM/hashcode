﻿using System.Collections.Generic;

namespace HashCode
{
    class Endpoint
    {
        public int Id;
        public int LatencyToDatabase;
        public List<Cache> Caches = new List<Cache>();
        public int NumberOfCaches;

        public int Count;
        public Endpoint(int id, int latency, int numberOfCaches)
        {
            Id = id;
            LatencyToDatabase = latency;
            NumberOfCaches = numberOfCaches;
        }
        public void addCache(Cache cache)
        {
            Caches[Count] = cache;
            Count++;
        }
    }
}
